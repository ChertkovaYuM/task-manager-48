package ru.tsc.chertkova.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.chertkova.tm.enumerated.Status;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import java.util.Date;

@Getter
@Setter
@MappedSuperclass
@NoArgsConstructor
public abstract class AbstractFieldsModel extends AbstractUserOwnerModel {

    @NotNull
    @Column(name = "name")
    protected String name = "";

    @NotNull
    @Column(name = "description")
    protected String description = "";

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    protected Status status = Status.NOT_STARTED;

    @Nullable
    @Column(name = "started_dt")
    protected Date dateBegin;

    @Nullable
    @Column(name = "completed_dt")
    private Date dateEnd;

}
